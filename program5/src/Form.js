import { useState } from 'react';

export default function Form() {

    // States for registration
    const [name, setName] = useState('');
    const [lname, setLName] = useState('');
    const [npi, setNpiNumber] = useState('');
    const [address, setAddress] = useState('');
    const [telNo, setTelephoneNO] = useState('');
    const [email, setEmail] = useState('');
    //const [password, setPassword] = useState('');

    // States for checking the errors
    const [submitted, setSubmitted] = useState(false);
    const [error, setError] = useState(false);

    // Handling the first name change
    const handleName = (e) => {
        setName(e.target.value);
        setSubmitted(false);
    };
    // Handling the last name change
    const handleLName = (e) => {
        setLName(e.target.value);
        setSubmitted(false);
    };
    // Handling the NPI Number change
    const handleNpiNumber = (e) => {
        setNpiNumber(e.target.value);
        setSubmitted(false);
    };

    // Handling Business Address change
    const handleAddress = (e) => {
        setAddress(e.target.value);
        setSubmitted(false);
    };
    // Handling Telephone Number change

    const handleTelephoneNO = (e) => {
        setTelephoneNO(e.target.value);
        setSubmitted(false);
    };

    // Handling the email change
    const handleEmail = (e) => {
        setEmail(e.target.value);
        setSubmitted(false);
    };

   
    // Handling the form submission
    const handleSubmit = (e) => {
        e.preventDefault();
        if (name === '' || lname === '' || npi === '' || address === '' || email === '') {
            setError(true);
        } else {
            setSubmitted(true);
            setError(false);
        }
    };

    // Showing success message
    const successMessage = () => {
        return (
            <div
                className="success"
                style={{
                    display: submitted ? '' : 'none',
                }}>
                <h1>User {name+' '+lname} successfully registered!!</h1>
            </div>
        );
    };

    // Showing error message if error is true
    const errorMessage = () => {
        return (
            <div
                className="error"
                style={{
                    display: error ? '' : 'none',
                }}>
                <h1>Please enter all the fields</h1>
            </div>
        );
    };

    return (
        <div className="form">
            <div>
                <h1>User Registration</h1>
            </div>

            {/* Calling to the methods */}
            <div className="messages">
                {errorMessage()}
                {successMessage()}
            </div>

            <form>
                {/* Labels and inputs for form data */}
                <label className="label">First Name</label>
                <input onChange={handleName} className="input"
                    value={name} type="text" />
                <label className="label">Last Name</label>
                <input onChange={handleLName} className="input"
                    value={lname} type="text" />

                <label className="label">NPI number</label>
                <input onChange={handleNpiNumber} className="input"
                    value={npi} type="text" />

                <label className="label">Business Address</label>
                <input onChange={handleAddress} className="input"
                    value={address} type="text" />

                <label className="label">Telephone Number</label>
                <input onChange={handleTelephoneNO} className="input"
                    value={telNo} type="text" />

                <label className="label">Email</label>
                <input onChange={handleEmail} className="input"
                    value={email} type="email" />

               
                <button onClick={handleSubmit} className="btn" type="submit">
                    Submit
                </button>
            </form>
        </div>
    );
}